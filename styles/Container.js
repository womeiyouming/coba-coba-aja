import styled from 'styled-components';

export const Container = styled.div`
    color: red;
    background-color: black;
    padding: 10px;
    &:hover {
        transition-duration: 1s;
        background-color: green;
    }
`